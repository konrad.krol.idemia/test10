FROM openjdk:11-alpine
ENTRYPOINT ["/usr/bin/test10.sh"]

COPY test10.sh /usr/bin/test10.sh
COPY target/test10.jar /usr/share/test10/test10.jar
